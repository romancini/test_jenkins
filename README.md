# AUA - Script to handle an order

This script have a base to make others. 
To create it, I used:
  - python 3.7
  - selenium 3.14.1
  - PhantomJS

### Installation

Download python and PhantomJS from websites. Selenium package could be installed from pip:

| Tool | Site |
| ------ | ------ |
| Python | [https://www.python.org/downloads/](https://www.python.org/downloads/) |
| PhamtonJS | [http://phantomjs.org/download.html](http://phantomjs.org/download.html) |

To install selenium (better in a virtualenv enviroment):
```sh
$ pip install -r requirements.txt
```